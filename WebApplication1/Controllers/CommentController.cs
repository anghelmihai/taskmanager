﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TaskManager.ViewModel;
using WebApplication1.Repository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class CommentController : Controller
    {
        private IDataRepository repository;

        public CommentController(IDataRepository _repository)
        {
            repository = _repository;
        }

        // GET: api/<controller>
        [HttpGet("[action]/{taskId}")]
        public IActionResult GetCommentsForTask(int taskId)
        {
            var task = repository.GetTask(taskId);
            if (task == null)
            {
                return BadRequest();
            }

            var results = repository.GetCommentsForTask(taskId);
            return Ok(results);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await repository.GetCommentAsync(id);
            if(result == null)
            {
                return BadRequest("Invalid id");
            }

            return Ok(result);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = repository.GetComments();
            return Ok(result);
        }

        // POST api/<controller>
        [HttpPost("{taskId}")]
        public IActionResult Post(int taskId, [FromBody]CommentModel comment)
        {
            var task = repository.GetTask(taskId);
            if (task == null)
            {
                return BadRequest("taskId is invalid");
            }

            if(comment == null)
            {
                return BadRequest("Can't insert null value");
            }

            comment.TaskId = taskId;
            repository.AddComment(comment);

            return Ok();
        }

        // PUT api/<controller>
        [HttpPut]
        public IActionResult Put([FromBody]CommentModel value)
        {
            if (value == null || value.Id == 0)
            {
                return BadRequest();
            }

            repository.UpdateComment(value);
            return Ok();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repository.DeleteComment(id); 
        }
    }
}
