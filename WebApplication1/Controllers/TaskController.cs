﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using TaskManager.Enum;
using TaskManager.ViewModel;
using WebApplication1.Repository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class TaskController : Controller
    {
        private IDataRepository repository;
        public TaskController(IDataRepository _repository)
        {
            repository = _repository;

            //var task = new TaskModel()
            //{
            //    DateCreated = DateTime.Now,
            //    DateRequired = DateTime.Now,
            //    AssignedTo = "Gigi",
            //    Status = TaskStatus.ToDO,
            //    Description = "aasa"
            //};

            //repository.AddTask(task);
        }

        [HttpGet]
        public IActionResult Tasks()
        {
            var results = repository.GetTasks();
            return Ok(results);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = repository.GetTask(id);

            if (result == null)
            {
                return NotFound("Invalid Id");
            }

            return Ok(result);
        }

        // POST api/<controller>
        [DisableCors]
        [HttpPost]
        public IActionResult Post([FromBody]TaskModel task)
        {
            if(task == null)
            {
                return BadRequest();
            }

            repository.AddTask(task);
            return Ok();
        }

        // PUT api/<controller>
        [DisableCors]
        [HttpPut]
        public IActionResult Put([FromBody]TaskModel task)
        {
            if(task == null || task.Id  == 0)
            {
                return BadRequest();
            }

            repository.UpdateTask(task);
            return Ok();
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repository.DeleteTask(id);
        }
    }
}
