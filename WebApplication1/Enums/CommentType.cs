﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Enum
{
    public enum CommentType
    {
        Comment = 0,
        ExtraSpec = 1,
        Question = 2
    }
}
