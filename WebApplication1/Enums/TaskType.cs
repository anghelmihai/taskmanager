﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Enum
{
    public enum TaskType
    {
        Task = 0,
        UserStory = 1,
        Bug = 2
    }
}
