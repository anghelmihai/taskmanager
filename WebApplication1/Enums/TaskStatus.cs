﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Enum
{
    public enum TaskStatus
    {
        ToDO = 0,
        Progress = 1,
        Testing = 2,
        Done = 3
    }
}
