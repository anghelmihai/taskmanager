"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Task = /** @class */ (function () {
    function Task() {
        this.dateCreated = new Date();
        this.dateRequired = new Date();
        this.nextActDate = new Date();
    }
    return Task;
}());
exports.Task = Task;
//# sourceMappingURL=task.js.map