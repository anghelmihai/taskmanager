export class Comment {
  id: number;

  taskId: number;
  dateAdded: Date;
  comment: string;
  type: string;
  remainderDate: Date;
}
