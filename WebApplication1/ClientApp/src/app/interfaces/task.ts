import { Comment } from "./comment";

export class Task {
  id: number;
  dateRequired: Date;
  dateCreated: Date;

  description: string;
  status: string;
  type: string;
  assignedTo: string;
  nextActDate: Date;
    comments: Comment[];

    constructor() {
        this.dateCreated = new Date();
        this.dateRequired = new Date();
        this.nextActDate = new Date();
    }
}
