import { Component, OnInit } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { TaskService } from '../services/task-service';
import { Task } from '../interfaces/task';
import { formatDate } from '@angular/common';
import { concat } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  tasks: Task[];
  cols: any[];
  taskTypes: string[];
  taskStatus: string[];
  newTaskForm: FormGroup;

  newTaskStatus: string;
  displayAddNew: boolean = false;
  constructor(private data: TaskService, private router: Router) { }

  ngOnInit() {

    this.taskTypes = ["Task", "User Story", "Bug"];
    this.taskStatus = ["To do", "In progress", "Testing", "Done"];

    this.data.getTasks().subscribe(data => {
      this.tasks = data;
      console.log(this.tasks);
      console.log(this.tasks[0].dateCreated);
    });

    this.cols = [
      { field: 'type', header: 'Type' },
      { field: 'dateCreated', header: 'Date Created' },
      { field: 'description', header: 'Description' },
      { field: 'assignedTo', header: 'Assigned To' },
      { field: 'status', header: 'Status' },
      { field: 'nextActDate', header: 'Next Date'},
      { field: 'dateRequired', header: 'Date Required' }
    ];
  }

  onSubmit(form: any) {
    console.log(form.value);
    //validations

    //Form and post
    var values = form.value;

    var taskToInsert = new Task();
    taskToInsert.description = values.description;
    taskToInsert.assignedTo = values.assignedTo;
    taskToInsert.status = values.status;
    taskToInsert.type = values.type;
    taskToInsert.dateRequired = values.dateRequired;
    taskToInsert.nextActDate = values.nextDate;
    taskToInsert.dateCreated = new Date();

    this.data.addTask(taskToInsert).subscribe(data => {
        console.log("Insert ok!");
        this.tasks.push(taskToInsert);
      this.displayAddNew = false;
    });
  }

  taskClicked(id: number) {
    console.log("clicked " + id);
    this.router.navigate(['../task-details', id]);
  }

  addNewTask() {
    this.displayAddNew = true;
    console.log("new task");
  }

    deleteTask(id: number) {
        this.data.removeTasks(id).subscribe(data => {
            console.log("Delete ok!");
            this.tasks.splice(id - 1, 1);
        });
  }

}
