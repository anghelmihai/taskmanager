import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  // Import it up here
import { Observable } from 'rxjs';
import { Task } from '../interfaces/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>('https://localhost:44397/api/Task/');
  }

  getTask(id : number): Observable<Task> {
    return this.http.get<Task>('https://localhost:44397/api/Task/' + id);
  }

  addTask(task: Task) {
    return this.http.post('https://localhost:44397/api/Task/', task);
  }

  putTask(task: Task) {
    return this.http.put('https://localhost:44397/api/Task/', task);
  }

  removeTasks(id: number) {
    return this.http.delete('https://localhost:44397/api/Task/' + id);
  }

}
