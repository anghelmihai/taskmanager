import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';
import { Task } from '../interfaces/task';
import { Comment } from '../interfaces/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

    getComments(): Observable<Comment[]> {
        return this.http.get<Comment[]>('https://localhost:44397/api/Comment/');
  }

    getCommentsForTask(taskId: number): Observable<Comment[]> {
        return this.http.get<Comment[]>('https://localhost:44397/api/Comment/GetCommentsForTask/' + taskId);
  }

    getComment(id: number): Observable<Comment> {
        return this.http.get<Comment>('https://localhost:44397/api/Comment/' + id);
  }

  addComment(idTask, comment: Comment) {
    return this.http.post('https://localhost:44397/api/Comment/' + idTask, comment);
  }

  putComment(comment: Comment) {
    return this.http.put('https://localhost:44397/api/Comment/', comment);
  }

  removeComment(id: number) {
    return this.http.delete('https://localhost:44397/api/Comment/' + id);
  }

  
}
