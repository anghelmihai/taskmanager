import { Component, OnInit } from '@angular/core';
import { CommentService } from '../services/comment-service';
import { Comment } from '../interfaces/comment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-comment',
  templateUrl: './search-comment.component.html',
  styleUrls: ['./search-comment.component.css']
})
export class SearchCommentComponent implements OnInit {
  comments: Comment[];
  commentTypes: string[];
  constructor(private commentService: CommentService, private router: Router) { }

  ngOnInit() {
    this.commentTypes = ["Comment", "Extra Specification", "Question"];

    this.commentService.getComments().subscribe(data => {
      this.comments = data;
      console.log(this.comments);
    });
  }

  onClick(idTask: number) {
    this.router.navigate(['../task-details', idTask]);
  }

}
