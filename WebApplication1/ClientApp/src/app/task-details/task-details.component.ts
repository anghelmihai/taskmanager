import { Component, OnInit, Input } from '@angular/core';
import { TaskService } from '../services/task-service';
import { Router } from '@angular/router';
import { Task } from '../interfaces/task';
import { Comment } from '../interfaces/comment'
import { CommentService } from '../services/comment-service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {
   idTask: any;
    actualTask: Task;
    commentsForTask: Comment[];
    taskTypes: string[];
    taskStatus: string[];
    defaultDate: Date;
    displayAddNew: boolean;
  commentTypes: string[];
  searchText: string;
  isInEdit: boolean;

    constructor(private taskService: TaskService, private router: Router,
        private commentService: CommentService, private route: ActivatedRoute) {
        this.actualTask = new Task();
        this.defaultDate = new Date();
    }

    ngOnInit() {
        this.taskTypes = ["Task", "User Story", "Bug"];
        this.taskStatus = ["To do", "In progress", "Testing", "Done"];
        this.commentTypes = ["Comment", "Extra Specification", "Question"];

        this.idTask = this.route.snapshot.paramMap.get('idTask');

        this.taskService.getTask(this.idTask).subscribe(task => {
            this.actualTask = task;
            this.defaultDate = new Date(this.actualTask.dateCreated);

            console.log(this.actualTask);

            this.commentService.getCommentsForTask(this.idTask).subscribe(comments => {
                this.commentsForTask = comments;
                console.log(this.commentsForTask);
            });
        });
    }

    addNewComment() {
        this.displayAddNew = true;
        console.log("new task");
    }

    deleteComment(comment: Comment) {
        this.commentService.removeComment(comment.id).subscribe(data => {
            console.log("Delete ok!");
            var indexOfComment = this.commentsForTask.indexOf(comment);
            if (indexOfComment != -1) {
                this.commentsForTask.splice(indexOfComment, 1);
            }
        });
    }

  saveEditTask() {
    this.isInEdit = true;

    this.taskService.putTask(this.actualTask).subscribe(data => {
      console.log("Saved!");
      this.isInEdit = false;
    });
  }

  saveEditComment(comment: Comment) {
    this.isInEdit = true;

    this.commentService.putComment(comment).subscribe(data => {
      console.log("Saved!");
      this.isInEdit = false;
    });
  }


    onSubmit(form: any) {
        console.log(form.value);
        //validations

        //Form and post
        var values = form.value;

        var commentToInsert = new Comment();
        commentToInsert.type = values.type;
        commentToInsert.remainderDate = values.remainderDate;
        commentToInsert.comment = values.comment;
        commentToInsert.dateAdded = new Date();

        this.commentService.addComment(this.idTask, commentToInsert).subscribe(data => {
            console.log("Insert ok!");
            this.commentsForTask.push(commentToInsert);
            this.displayAddNew = false;
        });
    }
}
