﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebApplication1.Context;

namespace WebApplication1.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TaskManager.ViewModel.CommentModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Comment");

                    b.Property<DateTime>("DateAdded");

                    b.Property<DateTime>("RemainderDate");

                    b.Property<int>("TaskId");

                    b.Property<int?>("TaskModelId");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("TaskModelId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("TaskManager.ViewModel.TaskModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AssignedTo");

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime>("DateRequired");

                    b.Property<string>("Description");

                    b.Property<DateTime>("NextActDate");

                    b.Property<string>("Status");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.ToTable("Tasks");
                });

            modelBuilder.Entity("TaskManager.ViewModel.CommentModel", b =>
                {
                    b.HasOne("TaskManager.ViewModel.TaskModel", "TaskModel")
                        .WithMany("Comments")
                        .HasForeignKey("TaskModelId");
                });
#pragma warning restore 612, 618
        }
    }
}
