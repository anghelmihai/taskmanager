﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using TaskManager.ViewModel;

namespace WebApplication1.Context
{

    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        { }

        public DbSet<TaskModel> Tasks { get; set; }
        public DbSet<CommentModel> Comments { get; set; }
    }
}