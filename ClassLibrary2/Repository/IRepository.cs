﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.ViewModel;

namespace WebApplication1.Repository
{
    public interface IDataRepository
    {
        void AddTask(TaskModel task);
        TaskModel GetTask(int Id);
        IEnumerable<TaskModel> GetTasks();
        void UpdateTask(TaskModel task);
        void DeleteTask(int id);

        void AddComment(CommentModel comment);
        IEnumerable<CommentModel> GetCommentsForTask(int taskId);
        Task<CommentModel> GetCommentAsync(int Id);
        void UpdateComment(CommentModel task);
        void DeleteComment(int id);
        IEnumerable<CommentModel> GetComments();
    }
}
