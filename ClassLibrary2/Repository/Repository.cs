﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.ViewModel;
using WebApplication1.Context;

namespace WebApplication1.Repository
{
    public class DataRepository : IDataRepository
    {
        private DataContext context;
        public DataRepository(DataContext _context)
        {
            context = _context;
        }

        #region Comments
        public void AddComment(CommentModel comment)
        {
            context.AddAsync(comment);
            context.SaveChanges();
        }

        public IEnumerable<CommentModel> GetCommentsForTask(int taskId)
        {
            var comments =  context.Set<CommentModel>().Where(c => c.TaskId == taskId);
            return comments;
        }

        public Task<CommentModel> GetCommentAsync(int Id)
        {
            return context.FindAsync<CommentModel>(Id);
        }

        public IEnumerable<CommentModel> GetComments()
        {
            return context.Set<CommentModel>().ToList();
        }

        public void DeleteComment(int id)
        {
            var itemToDel = context.Find<CommentModel>(id);
            context.Remove<CommentModel>(itemToDel);
            context.SaveChangesAsync();

        }

        public void UpdateComment(CommentModel comment)
        {
            var item = context.Find<CommentModel>(comment.Id);
            if (item == null)
            {
                comment.Id = 0;
                context.AddAsync(comment);
            }
            else
            {
                item.DateAdded = comment.DateAdded;
                item.RemainderDate = comment.RemainderDate;
                item.Comment = comment.Comment;
                item.Type = comment.Type;
                item.TaskId = comment.TaskId;
            }

            context.SaveChangesAsync();
        }
        #endregion

        #region Task
        public void AddTask(TaskModel task)
        {
            context.AddAsync(task);
            context.SaveChanges();
        }

        public IEnumerable<TaskModel> GetTasks()
        {
            return context.Set<TaskModel>();
        }

        public TaskModel GetTask(int Id)
        {
            return context.FindAsync<TaskModel>(Id).Result;
        }

        public void DeleteTask(int id)
        {
            var itemToDel = context.Find<TaskModel>(id);
            context.Remove<TaskModel>(itemToDel);
            context.SaveChangesAsync();
        }

        public void UpdateTask(TaskModel task)
        {
            var item = context.Find<TaskModel>(task.Id);
            if(item == null)
            {
                task.Id = 0;
                context.AddAsync(task);
            }
            else
            {
                item.DateCreated = task.DateCreated;
                item.DateRequired = task.DateRequired;
                item.AssignedTo = task.AssignedTo;
                item.Description = task.Description;
                item.NextActDate = task.NextActDate;
                item.Status = task.Status;
                item.Comments = task.Comments == null ? null : new List<CommentModel>(task.Comments);
            }

            context.SaveChangesAsync();
        }
        #endregion
    }
}
