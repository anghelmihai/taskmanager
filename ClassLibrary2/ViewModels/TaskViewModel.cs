﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaskManager.ViewModel
{
    public class TaskModel
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime DateRequired { get; set; }

        public string Description { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string AssignedTo { get; set; }
        public DateTime NextActDate { get; set; }

        public virtual IEnumerable<CommentModel> Comments { get; set; }
    }
}
