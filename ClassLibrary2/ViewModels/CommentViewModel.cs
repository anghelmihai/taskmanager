﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.ViewModel
{
    public class CommentModel
    {
        public int Id { get; set; }
        public int TaskId { get; set; }

        public DateTime DateAdded { get; set; }
        public string Comment { get; set; }
        public string Type { get; set; }
        public DateTime RemainderDate { get; set; }

        public virtual TaskModel TaskModel { get; set; }
    }
}
